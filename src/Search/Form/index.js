import React, { Component } from 'react';
import Autocomplete from './autocomplete';

class Index extends Component {

    constructor(props) {
        super(props);

        this.onLocationAutocompleteSelect = this.onLocationAutocompleteSelect.bind(this);

        this.state = {
            changes: {}
        };
    }

    // The fastest deep clone solution available
    cloneObject(data) {
        return JSON.parse(JSON.stringify(data))
    }

    onLocationAutocompleteSelect(value) {
        const state = this.cloneObject(this.state);

        state.changes = {
            ...state.changes,
            pickupLocation: value
        };

        return this.setState(state);
    }

    render() {
        return (
            <form className="searchbox-form" autoComplete="off">
                <h2 className="dark">
                    Where are you going?
                </h2>
                <Autocomplete name="pickupLocation"
                              autoComplete="off"
                              placeholder="city, airport, station, region and district..."
                              label="Pick-up Location"
                              onSelect={this.onLocationAutocompleteSelect}
                />
            </form>
        );
    }
}

export default Index;
