import React, { Component } from 'react';
import Input from '../../Shared/input';
import Option from './autocompleteOption';
import axios from 'axios/index';

const autocompletePlaceTypeNameMap = {
    'A': {
        title: 'Airport',
        style: {
            backgroundColor: 'rgb(150, 20, 18)'
        }
    },
    'D': {
        title: 'District',
        style: {
            backgroundColor: 'rgb(1, 124, 68)'
        }
    },
    'C': {
        title: 'City',
        style: {
            backgroundColor: 'rgb(10, 99, 176)'
        }
    },
    'T': {
        title: 'Station',
        style: {
            backgroundColor: 'rgb(91, 91, 91)'
        }
    }
};

class Autocomplete extends Component {

    constructor(props) {
        super(props);

        this.requestLocationAutocompleteData = this.requestLocationAutocompleteData.bind(this);
        this.changePickupLocationsOptions = this.changePickupLocationsOptions.bind(this);
        this.parseAutocompleteOptionsFromAPI = this.parseAutocompleteOptionsFromAPI.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
        this.onOptionClick = this.onOptionClick.bind(this);

        this.timeout =  0;
        this.state = {
            value:                     '',
            isFetchingLocationOptions: false,
            pickupLocationOptions:     []
        };
    }

    async onInputChange(value) {
        this.setState({
            ...this.state,
            value,
            isFetchingLocationOptions: (value && value.length > 1),
            pickupLocationOptions: (value && value.length > 1) ? this.state.pickupLocationOptions : []
        });

        // Proceed to API only if there are at least 2 characters typed in
        if (value && value.length > 1) {
            await this.changePickupLocationsOptions(value);
        }
    }
    
    async changePickupLocationsOptions(value) {
        // Throttle the API to improve user experience
        if (this.timeout) {
            clearTimeout(this.timeout);
        }

        this.timeout = await setTimeout(async () => {
            const response = await this.requestLocationAutocompleteData(value);

            // Check if the input is not currently empty before updating with results
            if (this.state.value.length < 2) {
                return this.setState({
                    ...this.state,
                    isFetchingLocationOptions: false,
                    pickupLocationOptions: []
                });
            }

            this.setState({
                ...this.state,
                isFetchingLocationOptions: false
            });

            if (!response || (response && response.status !== 200)) {
                return;
            }

            // Due to unknown API spec. silence any error
            const {data} = response;
            const {results} = data;

            if (!results) {
                return;
            }

            const {docs, numFound} = results;

            if (numFound === 0) {
                return this.setState({
                    ...this.state,
                    pickupLocationOptions: []
                });
            }

            const pickupLocationOptions = this.parseAutocompleteOptionsFromAPI(docs);

            this.setState({
                ...this.state,
                pickupLocationOptions: pickupLocationOptions
            });
        }, 800);
    }

    async requestLocationAutocompleteData(value) {
        return axios.get(
            'https://cors.io/?https://www.rentalcars.com/FTSAutocomplete.do?solrIndex=fts_en&solrRows=6&solrTerm=' + value
        );
    }

    parseAutocompleteOptionsFromAPI(options) {
        return options.map(option => {
            const {city,
                region,
                country,
                iata,
                name,
                placeType,
                locationId} = option;

            const cityOrRegion = city || region;
            let labelTitle = name;
            let labelSubTitle = [];

            if (iata) {
                labelTitle += ' ('+ iata +')';
            }

            if (cityOrRegion) {
                labelSubTitle.push(cityOrRegion);
            }

            if (country) {
                labelSubTitle.push(country);
            }

            return {
                value: locationId,
                labelTitle: Autocomplete.markSearchValue(labelTitle, this.state.value),
                labelSubTitle: labelSubTitle.join(', '),
                typeData: autocompletePlaceTypeNameMap[placeType]
            };
        });
    }

    static markSearchValue(stringToReplace, value) {
        if (Boolean(value) === false) {
            return stringToReplace;
        }

        const title = stringToReplace.toLowerCase();
        const matchIndex = title.indexOf(value.toLowerCase());

        if (matchIndex === -1) {
            return stringToReplace;
        }

        const match = stringToReplace.substr(matchIndex, value.length);

        return stringToReplace.replace(match, '<mark>' + match + '</mark>');
    }

    onOptionClick(value) {
        return this.props.onSelect(value);
    }

    renderOptions() {
        if (this.state.value.length < 2 || this.state.isFetchingLocationOptions === true) {
            return null;
        }

        if (
            !this.state.pickupLocationOptions
            || (this.state.pickupLocationOptions && this.state.pickupLocationOptions.length === 0)
        ) {
            const opt = {
                labelTitle: 'No Results Found'
            };

            return (
                <table className="table table-hover searchbox-results">
                    <tbody>
                        <Option {...opt} key="no-results" />
                    </tbody>
                </table>
            );
        }

        const options = this.state.pickupLocationOptions.map((option, key) => {
            return (
                <Option {...option} key={key} onClick={this.onOptionClick} />
            );
        });

        return (
            <table className="table table-hover searchbox-results">
                <tbody>
                    {options}
                </tbody>
            </table>
        );
    }

    render() {
        const inputClassName = this.state.isFetchingLocationOptions ? 'loading' : null;

        return (
            <div>
                <label className="margin-bottom-0">
                    {this.props.label}
                </label>
                <div>
                    <Input
                        {...this.props}
                        className={inputClassName}
                        type="text"
                        onInputChange={this.onInputChange}
                        value={this.state.value}
                    />
                </div>
                {this.renderOptions()}
            </div>
        );
    }
}

export default Autocomplete;
