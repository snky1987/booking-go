import React, { Component } from 'react';

class AutocompleteOption extends Component {

    constructor(props) {
        super(props);

        this.onOptionClick = this.onOptionClick.bind(this);
    }

    onOptionClick() {
        this.props.onClick(this.props.value);
    }

    renderTypeCellContent() {
        const typeData = this.props.typeData;

        if (typeData === undefined) {
            return null;
        }

        return (
            <div className="btn" style={typeData.style}>
                {typeData.title}
            </div>
        );
    }

    render() {
        return (
            <tr className="table table-sm table-hover" onClick={this.onOptionClick}>
                <th className="pill" scope="row">
                    {this.renderTypeCellContent()}
                </th>
                <td>
                    <span className="searchbox-results-title"
                          dangerouslySetInnerHTML={
                              {
                                  __html: this.props.labelTitle
                              }
                          }
                    />
                    <div className="searchbox-results-sub-title">
                        {this.props.labelSubTitle}
                    </div>
                </td>
            </tr>
        );
    }
}

export default AutocompleteOption;
