import React from "react";
import { create } from "react-test-renderer";
import Form from "./index";

describe("Form component", () => {
    test("it creates component as form element and must not auto complete", () => {
        const component = create(<Form />);
        const componentJSON = component.toJSON();

        expect(componentJSON.type).toEqual('form');
        expect(componentJSON.props.autoComplete).toEqual('off');
    });
});