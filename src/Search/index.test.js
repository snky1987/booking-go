import React from "react";
import { create } from "react-test-renderer";
import Search from "./index";

describe("Search component", () => {
    test("it creates component as div element and render the form", () => {
        const component = create(<Search />);
        const componentJSON = component.toJSON();

        expect(componentJSON.type).toEqual('div');
        expect(componentJSON.children[0].type).toEqual('form');
    });
});