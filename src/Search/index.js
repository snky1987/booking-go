import React, { Component } from 'react';
import Form from './Form/index'

class Search extends Component {
    render() {
        return (
            <div className="searchbox container">
                <Form />
            </div>
        );
    }
}

export default Search;
