import React, { Component } from 'react';

class Input extends Component {

    constructor(props) {
        super(props);

        this.onInputChange = this.onInputChange.bind(this);
    }

    onInputChange(evt) {
        return this.props.onInputChange(evt.target.value);
    }

    render() {
        return (
            <div>
                <input className={this.props.className}
                       type={this.props.type}
                       name={this.props.name}
                       autoComplete={this.props.autoComplete}
                       placeholder={this.props.placeholder}
                       value={this.props.value}
                       onChange={this.onInputChange}
                />
            </div>
        );
    }
}

export default Input;
