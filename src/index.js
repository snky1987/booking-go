import React from 'react';
import ReactDOM from 'react-dom';
import Search from './Search';

// CSS
import 'bootstrap/dist/css/bootstrap.css';
import './../public/stylesheets/pickup-location-box.css';

ReactDOM.render(
  <Search />,
  document.getElementById('root')
);
