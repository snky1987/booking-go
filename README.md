# Booking Go

Booking Go Technical Test - Michal Malinowski

## Assumptions
1. Developer was not asked to replicate (even as wireframe) rentalcars.com website and this application is an Widget that can be inserted into any front-end after calibration.
2. Developer was not asked to replicate entire widget from rentalcars.com website but to create only this part of the widget and functionality described in test spec.
3. Developer was not asked to unit test the widget.

## Dev Introduction
For the purpose of this test I decided to use React written in ES6.

Due to the fact that I am Javascript developer with full-stack skill-set I decided to use Bootstrap 4 for stylesheets which I modified slightly to better match rentalcars.com styling. Bootstrap 4 is also by default optimized for mobile devices.

Application created for this test is a responsive widget that can be implemented to any front-end also using micro front-ends architecture (like for example in Spotify).

I made an decision to do not use Redux in this test to better illustrate my bi-directional coding standards. However, if the application would be considered to be bigger then using Redux is a natural choice.

I did not decided to write any middleware API because data API was provided and an API would be an overkill.

## Installation
In order to run this Widget program please follow steps:
1. Clone this repository to local machine
2. Inside repository directory run: npm install
3. Inside repository directory run: npm start
4. Action will be followed by either opening the browser or a new tab with address
http://localhost:3000/

## Routes
"Widget" nature of this application did not require to build routes.
Widget is available in the browser as: http://localhost:3000/

## Testing
Tests can be run with command: npm test

Every ReactJS application can be easily tested. However testing always extend the time required to deliver the software. 

For the purpose of this test I decided not to include only few very elementary tests.